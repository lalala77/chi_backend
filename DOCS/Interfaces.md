### 用户

| 接口     | https://wx.naughtycat.xyz/user/login     |
| -------- | ---------------------------------------- |
| 参数     | {"username": "xxx", "password": "xxx"}   |
| 请求方式 | POST                                     |
| 备注     | username与password均不为空值（前端判断） |

| 接口     | https://wx.naughtycat.xyz/user/register  |
| -------- | ---------------------------------------- |
| 参数     | {"username": "xxx", "password": "xxx"}   |
| 请求方式 | POST                                     |
| 备注     | username与password均不为空值（前端判断） |

### 餐厅

| 接口     | https://wx.naughtycat.xyz/restaurant |
| -------- | ------------------------------------ |
| 参数     | 无                                   |
| 请求方式 | GET                                  |
| 备注     |                                      |

### 评论

| 接口     | https://wx.naughtycat.xyz/comment                            |
| -------- | ------------------------------------------------------------ |
| 参数     | {<br/>"uid": "提交该评价的用户id", <br>"rid": "该评价对应的餐厅id", <br/>"score": "评分", <br/>"content": "评价内容"<br/>} |
| 请求方式 | POST                                                         |
| 备注     |                                                              |

| 接口     | https://wx.naughtycat.xyz/comment                            |
| -------- | ------------------------------------------------------------ |
| 参数     | {<br/>“uid”: "提交该评价的用户id", <br/>"rid": "该评价对应的餐厅id",<br/> "scoreLB": "评分最小值", <br/>"scoreUB": "评分最大值",<br/> "tsLB": "发布评论时的时间戳最小值", <br/>"tsUB": "发布评论时的时间戳最大值"<br/>} |
| 请求方式 | GET                                                          |
| 备注     |                                                              |