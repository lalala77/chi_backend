package xyz.naughtycat.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.naughtycat.model.co.CommentCO;
import xyz.naughtycat.model.vo.CommentVO;
import xyz.naughtycat.service.CommentService;
import xyz.naughtycat.web.Response;

import java.util.List;

@RestController
@RequestMapping(path = "/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/add")
    public Response postComment(@RequestBody CommentVO commentVO) {
        commentService.postAComment(commentVO);
        return Response.buildSuccess();
    }

    @PostMapping("/query")
    public Response getByCondition(@RequestBody CommentCO commentCo) {
        List<CommentVO> vos = commentService.getCommentByCondition(commentCo);
        return Response.buildSuccess(vos);
    }

}
