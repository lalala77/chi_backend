package xyz.naughtycat.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.naughtycat.model.co.DishCO;
import xyz.naughtycat.model.vo.DishVO;
import xyz.naughtycat.service.DishService;
import xyz.naughtycat.web.Response;

import java.util.List;

@RestController
@RequestMapping(path = "/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    @PostMapping("/query")
    public Response getByCondition(@RequestBody DishCO dishCO) {
        List<DishVO> vos = dishService.getDishByCondition(dishCO);
        return Response.buildSuccess(vos);
    }
}
