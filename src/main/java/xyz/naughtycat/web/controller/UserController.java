package xyz.naughtycat.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.naughtycat.model.vo.UserVO;
import xyz.naughtycat.service.UserService;
import xyz.naughtycat.web.Response;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public Response userLogin(@RequestBody UserVO userVO) {
        return Response.buildSuccess(userService.login(userVO));
    }

    @GetMapping("/star/{username}")
    public Response getStars(@PathVariable("username") String username) {
        return Response.buildSuccess(userService.getStars(username));
    }

    @GetMapping("/star/{username}/{did}")
    public Response addStars(@PathVariable("username") String username, @PathVariable("did") String did) {
        userService.addStars(username, did);
        return Response.buildSuccess();
    }
    @GetMapping("/sign/{username}")
    public Response getSign(@PathVariable("username") String username) {
        return Response.buildSuccess(userService.getSign(username));
    }
    @GetMapping("/avatar/{username}")
    public Response getAvatar(@PathVariable("username") String username) {
        return Response.buildSuccess(userService.getAvatar(username));
    }
    @PostMapping("/sign")
    public Response updateSign(@RequestBody UserVO userVO) {
        userService.updateSign(userVO);
        return Response.buildSuccess();
    }



}
