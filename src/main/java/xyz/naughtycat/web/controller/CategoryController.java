package xyz.naughtycat.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.naughtycat.model.co.CommentCO;
import xyz.naughtycat.model.vo.CommentVO;
import xyz.naughtycat.service.CommentService;
import xyz.naughtycat.web.Response;

import java.util.List;

@RestController
@RequestMapping(path = "/category")
public class CategoryController {
    @Autowired
    private CommentService commentService;

    @PostMapping("/add")
    public Response postComment(@RequestBody CommentVO commentVO) {
        commentService.postAComment(commentVO);
        return Response.buildSuccess();
    }

    @PostMapping("/query")
    public Response getByCondition(@RequestBody CommentCO co) {
        List<CommentVO> vos = commentService.getCommentByCondition(co);
        return Response.buildSuccess(vos);
    }
}
