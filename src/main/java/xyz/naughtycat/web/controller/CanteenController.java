package xyz.naughtycat.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.naughtycat.service.CanteenService;
import xyz.naughtycat.web.Response;


@RestController
@RequestMapping(path = "/canteen")
public class CanteenController {
    @Autowired
    private CanteenService canteenService;

    @GetMapping
    public Response queryAllCanteens() {
        return Response.buildSuccess(canteenService.queryAllCategory());
    }

}
