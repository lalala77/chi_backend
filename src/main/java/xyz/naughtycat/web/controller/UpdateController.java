package xyz.naughtycat.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.naughtycat.service.UpdateService;
import xyz.naughtycat.web.Response;

@RestController
@RequestMapping(path = "/update")
public class UpdateController {
    @Autowired
    private UpdateService updateService;

    @GetMapping()
    public Response updateAll() {
        updateService.updateAll();
        return Response.buildSuccess();
    }
}
