package xyz.naughtycat.service;

import xyz.naughtycat.model.vo.DishVO;
import xyz.naughtycat.model.vo.UserVO;

import java.util.List;

public interface UserService {

    /**
     * 用户登录
     * @param userVO
     * @return
     */
    Integer login(UserVO userVO);


    /**
     * 获得收藏菜品
     */
    List<DishVO> getStars(String username);

    /**
     * 添加收藏菜品
     */
    void addStars(String username, String did);

    String getUsernameById(Integer uid);

    String getSign(String username);

    void updateSign(UserVO userVO);

    String getAvatar(String username);
}
