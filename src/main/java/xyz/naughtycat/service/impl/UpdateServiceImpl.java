package xyz.naughtycat.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.naughtycat.model.co.CommentCO;
import xyz.naughtycat.model.co.DishCO;
import xyz.naughtycat.model.vo.CommentVO;
import xyz.naughtycat.model.vo.DishVO;
import xyz.naughtycat.service.CommentService;
import xyz.naughtycat.service.DishService;
import xyz.naughtycat.service.UpdateService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UpdateServiceImpl implements UpdateService {

    @Autowired
    private DishService dishService;
    @Autowired
    private CommentService commentService;

    @Override
    public void updateAll() {
        Map<Integer, Score> record = new HashMap<>();
        List<CommentVO> commentByCondition = commentService.getCommentByCondition(new CommentCO());
        for (CommentVO commentVO : commentByCondition) {
            Integer did = commentVO.getDid();
            Score score;
            if(!record.containsKey(did)){
                score = new Score();
            }
            else{
                score = record.get(did);
            }
            score.addScore(commentVO.getScore());
            record.put(did, score);
        }
        DishCO dishCO = new DishCO();
        dishCO.setNum(Integer.MAX_VALUE);
        List<DishVO> dishByCondition = dishService.getDishByCondition(dishCO);
        for (DishVO dishVO : dishByCondition) {
            Integer did = dishVO.getId();
            Float score;
            if(!record.containsKey(did)){
                score = 0.0f;
            }
            else{
                score = record.get(did).getScore();
            }
            dishService.updateScoreById(did, score);
        }
    }
}

class Score {

    private Float scoreSum = 0.0f;
    private Integer count = 0;

    public void addScore(Float score){
        scoreSum += score;
        count += 1;
    }

    public Float getScore(){
        return scoreSum / count;
    }
}
