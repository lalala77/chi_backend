package xyz.naughtycat.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.naughtycat.dao.UserMapper;
import xyz.naughtycat.model.po.User;
import xyz.naughtycat.model.vo.DishVO;
import xyz.naughtycat.model.vo.UserVO;
import xyz.naughtycat.service.DishService;
import xyz.naughtycat.service.UserService;

import java.util.List;
import java.util.Objects;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DishService dishService;

    @Override
    public Integer login(UserVO userVO) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", userVO.getUsername());
        User user = userMapper.selectOne(wrapper);
        if (null == user) {
            user = register(userVO);
            return login(userVO);
        }
        return user.getId();
    }

    private User register(UserVO userVO) {
        User userSave = new User();
        BeanUtils.copyProperties(userVO, userSave);
        userSave.setPhone("None");
        userSave.setSignupTime(System.currentTimeMillis());
        int insert = userMapper.insert(userSave);
        userSave.setId(insert);
        return userSave;
    }

    @Override
    public List<DishVO> getStars(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        User user = userMapper.selectOne(wrapper);
        if (user == null) {
            return null;
        } else {
            String star = user.getStar();
            if ("".equals(star) || null == star) {
                return null;
            } else {
                String[] split = user.getStar().split(";");
                return dishService.selectByIds(split);
            }
        }
    }

    @Override
    public void addStars(String username, String did) {
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("username", username);

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        User user = userMapper.selectOne(wrapper);
        if (null == user) {
            UserVO userVO = new UserVO();
            userVO.setUsername(username);
            register(userVO);
            user = userMapper.selectOne(wrapper);
        }
        String star = user.getStar();

        User userNew = new User();
        if (Objects.equals(star, "") || null == star) {
            userNew.setStar(String.valueOf(did));
        } else {
            boolean exist = false;
            StringBuilder starNew = new StringBuilder();
            for (String s : star.split(";")) {
                if (Objects.equals(s, String.valueOf(did))) {
                    exist = true;
                } else {
                    if (starNew.length() != 0) {
                        starNew.append(";");
                    }
                    starNew.append(s);
                }
            }
            if (!exist) {
                starNew.append(";");
                starNew.append(did);
            }
            userNew.setStar(starNew.toString());
        }
        userMapper.update(userNew, updateWrapper);
    }

    @Override
    public String getUsernameById(Integer uid) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("id", uid);
        User user = userMapper.selectOne(wrapper);
        if (user == null) {
            return "该用户已注销";
        } else {
            return user.getUsername();
        }
    }

    @Override
    public String getSign(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        User user = userMapper.selectOne(wrapper);
        if (user == null) {
            return null;
        } else {
            String sign = user.getSign();
            if ("".equals(sign) || null == sign) {
                return null;
            } else {
                return sign;
            }
        }
    }

    @Override
    public void updateSign(UserVO userVO) {
        System.out.println(userVO.getSign());
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", userVO.getUsername());
        User user = userMapper.selectOne(wrapper);
        if (user == null) {
            throw new RuntimeException("User not found.");
        }
        user.setSign(userVO.getSign());
        int rowsAffected = userMapper.update(user, wrapper);
        if (rowsAffected > 0) {
            System.out.println("User information updated successfully.");
        } else {
            System.out.println("User information update failed.");
        }
    }

    @Override
    public String getAvatar(String username) {
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("username", username);
        User user = userMapper.selectOne(wrapper);
        if (user == null) {
            return null;
        } else {
            String sign = user.getAvatarUrl();
            if ("".equals(sign) || null == sign) {
                return null;
            } else {
                return sign;
            }
        }
    }
}
