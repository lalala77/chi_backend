package xyz.naughtycat.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.naughtycat.dao.DishMapper;
import xyz.naughtycat.model.co.DishCO;
import xyz.naughtycat.model.po.Dish;
import xyz.naughtycat.model.vo.DishVO;
import xyz.naughtycat.service.DishService;

import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private DishMapper dishMapper;

    @Override
    public List<DishVO> getDishByCondition(DishCO dishCO) {
        QueryWrapper<Dish> wrapper = new QueryWrapper<>();
        IPage<Dish> page = new Page<>();
        /* 菜品id不为空 */
        if(dishCO.getDid() != null){
            wrapper.eq("id", dishCO.getDid());
        }
        /* 搜索关键词不为空 */
        if(dishCO.getKeyWord() != null) {
            wrapper.like("name", dishCO.getKeyWord());
        }
        /* 食堂id不为空 */
        if(dishCO.getCid() != null) {
            wrapper.eq("cid", dishCO.getCid());
        }
        /* 指定排序规则 */
        if (dishCO.getByPrice() != null && dishCO.getByPrice()) {
            wrapper.orderByAsc("prize");
        } else if (dishCO.getByScore() != null && dishCO.getByScore()) {
            wrapper.orderByDesc("score");
        } else {
            wrapper.orderByAsc("name");
        }
        /* 分页 */
        int size = (dishCO.getNum() == null) ? 10 : dishCO.getNum();
        int current = (dishCO.getCurrent() == null) ? 0 : dishCO.getCurrent();
        page.setSize(size);
        page.setCurrent(current);
        dishMapper.selectPage(page, wrapper);
        List<Dish> pos = page.getRecords();
        List<DishVO> vos = new ArrayList<>();
        for (Dish po : pos) {
            DishVO vo = new DishVO();
            BeanUtils.copyProperties(po, vo);
            String indexImgUrl = po.getImgUrl().split(";")[0];
            vo.setIndexImgUrl(indexImgUrl);
            vos.add(vo);
        }
        return vos;
    }

    @Override
    public List<DishVO> selectByIds(String[] ids) {
        List<DishVO> result = new ArrayList<>();
        for (String id : ids) {
            Dish po = dishMapper.selectById(id);
            DishVO vo = new DishVO();
            BeanUtils.copyProperties(po, vo);
            String indexImgUrl = po.getImgUrl().split(";")[0];
            vo.setIndexImgUrl(indexImgUrl);
            result.add(vo);
        }
        return result;
    }

    @Override
    public void updateScoreById(Integer id, Float score) {
        UpdateWrapper<Dish> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id", id);
        Dish dish = new Dish();
        dish.setScore(score);
        dishMapper.update(dish, updateWrapper);
    }
}
