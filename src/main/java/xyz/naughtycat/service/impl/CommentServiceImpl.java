package xyz.naughtycat.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.naughtycat.dao.CommentMapper;
import xyz.naughtycat.model.co.CommentCO;
import xyz.naughtycat.model.po.Comment;
import xyz.naughtycat.model.vo.CommentVO;
import xyz.naughtycat.model.vo.UserVO;
import xyz.naughtycat.service.CommentService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private UserServiceImpl userService;

    @Override
    public void postAComment(CommentVO vo) {
        Comment po = new Comment();
        BeanUtils.copyProperties(vo, po);
        if(null != vo.getUsername()) {
            UserVO userVO = new UserVO();
            userVO.setUsername(vo.getUsername());
            Integer uid = userService.login(userVO);
            po.setUid(uid);
            
        }
        po.setTs(System.currentTimeMillis());
        commentMapper.insert(po);
    }

    @Override
    public List<CommentVO> getCommentByCondition(CommentCO co) {
        QueryWrapper<Comment> wrapper = new QueryWrapper<>();
        if(null != co.getUid()) {
            wrapper.eq("uid", co.getUid());
        }
        if(null != co.getUsername()) {
            UserVO userVO = new UserVO();
            userVO.setUsername(co.getUsername());
            Integer uid = userService.login(userVO);
            wrapper.eq("uid", uid);
        }
        if(null != co.getDid()) {
            wrapper.eq("did", co.getDid());
        }
        if(null != co.getScoreLB()) {
            wrapper.ge("score", co.getScoreLB());
        }
        if(null != co.getScoreUB()) {
            wrapper.le("score", co.getScoreUB());
        }
        if(null != co.getTsLB()) {
            wrapper.ge("ts", co.getTsLB());
        }
        if(null != co.getTsUB()) {
            wrapper.le("ts", co.getTsUB());
        }
        if(null != co.getAvatarUrl()){
            wrapper.eq("avatarUrl",co.getAvatarUrl());
        }
        List<Comment> pos = commentMapper.selectList(wrapper);
        List<CommentVO> result = new ArrayList<>();
        for (Comment po : pos) {
            CommentVO vo = new CommentVO();
            BeanUtils.copyProperties(po, vo);
            String username = userService.getUsernameById(po.getUid());
            vo.setUsername(username);
            result.add(vo);
        }
        return result;
    }
}
