package xyz.naughtycat.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.naughtycat.dao.CanteenMapper;
import xyz.naughtycat.model.po.Canteen;
import xyz.naughtycat.model.vo.CanteenVO;
import xyz.naughtycat.service.CanteenService;

import java.util.ArrayList;
import java.util.List;

@Service
public class CanteenServiceImpl implements CanteenService {

    @Autowired
    private CanteenMapper CanteenMapper;

    @Override
    public List<CanteenVO> queryAllCategory() {
        List<Canteen> Canteens = CanteenMapper.selectList(null);
        List<CanteenVO> result = new ArrayList<>();
        for (Canteen po : Canteens) {
            CanteenVO vo = new CanteenVO();
            BeanUtils.copyProperties(po, vo);
            result.add(vo);
        }
        return result;
    }

}
