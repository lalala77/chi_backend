package xyz.naughtycat.service;

import xyz.naughtycat.web.Response;

public interface UpdateService {
    void updateAll();
}
