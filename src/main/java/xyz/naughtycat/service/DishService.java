package xyz.naughtycat.service;

import xyz.naughtycat.model.co.DishCO;
import xyz.naughtycat.model.vo.DishVO;

import java.util.List;

public interface DishService {

    List<DishVO> getDishByCondition(DishCO dishCO);

    List<DishVO> selectByIds(String[] ids);

    void updateScoreById(Integer id, Float score);
}
