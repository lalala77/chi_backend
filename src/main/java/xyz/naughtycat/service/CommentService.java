package xyz.naughtycat.service;

import xyz.naughtycat.model.co.CommentCO;
import xyz.naughtycat.model.po.Comment;
import xyz.naughtycat.model.vo.CommentVO;

import java.util.List;

public interface CommentService {

    /**
     * 发布评论
     */
    void postAComment(CommentVO vo);

    /**
     * 按照条件查询评论
     */
    List<CommentVO> getCommentByCondition(CommentCO co);
}
