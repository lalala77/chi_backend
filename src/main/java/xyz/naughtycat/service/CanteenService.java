package xyz.naughtycat.service;

import xyz.naughtycat.model.vo.CanteenVO;

import java.util.List;

public interface CanteenService {

    /**
     * 查询餐厅
     * @return 所有餐厅信息
     */
    List<CanteenVO> queryAllCategory();

}
