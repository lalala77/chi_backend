package xyz.naughtycat.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Comment {

    /* 自增PK */
    private Integer id;

    /* 提交该评价的用户id */
    private Integer uid;

    /* 该评价对应的菜品id */
    private Integer did;

    /* 该评价对应的菜品id */
    private Float score;

    /* 评价内容 */
    private String content;

    /* 发布评论时的时间戳 */
    private Long ts;

    private String avatarUrl;

}

