package xyz.naughtycat.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Dish {

    /* 自增PK */
    private Integer id;

    /* 食堂ID */
    private Integer cid;

    /* 菜品名称 */
    private String name;

    /* 菜品价格 */
    private Integer price;

    /* 评分 */
    private Float score;

    /* 按份计费/称重 */
    private String type;

    /* 图片链接 */
    private String imgUrl;

}
