package xyz.naughtycat.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    /* 自增PK */
    private Integer id;

    /* 用户名 */
    private String username;

    /* 手机号 */
    private String phone;

    /* 收藏菜品编号 */
    private String star;

    /* 注册时间 */
    private Long signupTime;

    /*用户头像 */
    private String avatarUrl;
    /* 用户个性签名*/
    private String sign;
}
