package xyz.naughtycat.model.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Canteen {

    /* 自增PK */
    private Integer id;

    /* 餐厅名称 */
    private String name;

    /* 营业时间 */
    private String businessHours;

    /* 所属校区 */
    private String school;

}
