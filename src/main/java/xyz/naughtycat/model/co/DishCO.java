package xyz.naughtycat.model.co;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DishCO {

    /* 菜品id */
    private Integer did;

    /* 搜索关键词 */
    private String keyWord;

    /* 按照价格升序排序 */
    private Boolean byPrice;

    /* 按照评分降序排序 */
    private Boolean byScore;

    /* 当前页数 */
    private Integer current;

    /* 页面容量 */
    private Integer num;

    /* 所属食堂id */
    private Integer cid;
}
