package xyz.naughtycat.model.co;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 筛选评论的条件
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentCO {
    private String username;    // 提交改评价的用户名
    private Integer uid;        // 提交该评价的用户id
    private Integer did;        // 该评价对应的菜品id
    private Float scoreLB;      // 评分最小值
    private Float scoreUB;      // 评分最大值
    private Long tsLB;          // 发布评论时的时间戳最小值
    private Long tsUB;          // 发布评论时的时间戳最大值
    private String avatarUrl;
}
