package xyz.naughtycat.model.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserVO {

    /* 自增PK */
    private Integer id;

    /* 用户名 */
    private String username;

    /* 手机号 */
    private String phone;

    /* 收藏菜品编号 */
    private String star;

    /*用户头像 */
    private String avatarUrl;
    /* 用户个性签名*/

    private String sign;

}
