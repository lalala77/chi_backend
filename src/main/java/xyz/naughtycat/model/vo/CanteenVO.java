package xyz.naughtycat.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CanteenVO {

    /* 自增PK */
    private Integer id;

    /* 餐厅名称 */
    private String name;

    /* 营业时间 */
    private String businessHours;

    /* 所属校区 */
    private String school;
}
