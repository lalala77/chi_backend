package xyz.naughtycat.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DishVO {

    /* 自增PK */
    private Integer id;

    /* 名称 */
    private String name;

    /* 位置 */
    private Integer cid;

    /* 价格 */
    private Integer price;

    /* 评分 */
    private Float score;

    /* 按份计费/称重 */
    private String type;

    /* 首页图片链接 */
    private String indexImgUrl;

    /* 详情图片链接 */
    private String imgUrl;

}
