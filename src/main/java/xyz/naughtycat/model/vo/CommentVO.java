package xyz.naughtycat.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CommentVO {
    private Integer uid;        // 提交该评价的用户id
    private String username;    // 提交该评价的用户id
    private Integer did;        // 该评价对应的菜品id
    private Float score;        // 评分
    private String content;     // 评价内容
    private Long ts;            // 发布评论时的时间戳
    private String avatarUrl;
}
