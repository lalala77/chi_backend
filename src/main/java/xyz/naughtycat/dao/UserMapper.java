package xyz.naughtycat.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.naughtycat.model.po.User;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    int insert(User entity);
}
