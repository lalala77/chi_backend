package xyz.naughtycat.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.naughtycat.model.po.Comment;

@Mapper
public interface CommentMapper extends BaseMapper<Comment> {
}
