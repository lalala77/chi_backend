package xyz.naughtycat.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.naughtycat.model.po.Canteen;

@Mapper
public interface CanteenMapper extends BaseMapper<Canteen> {

}
