package xyz.naughtycat.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.naughtycat.model.po.Dish;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {}
