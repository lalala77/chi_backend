package xyz.naughtycat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class wxApplication {

	public static void main(String[] args) {
		SpringApplication.run(wxApplication.class, args);
	}

}
